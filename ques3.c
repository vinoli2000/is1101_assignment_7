#include<stdio.h>

void main()
{
	int a[3][3], b[3][3], c[3][3]={0}, d[3][3]={0};
	int i,j,k,m,n,p,q;
	printf("Enter no. of rows and columns in matrix 1: ");
	scanf("%d%d",&m,&n);
	printf("Enter no. of rows and columns in matrix 2: ");
	scanf("%d%d",&p,&q);
	if(m!=p || n!=q)
	{
		printf("Cannot add two matrices. Do another input");
		return;
	}
	else if(n!=p)
	{
		printf("Cannot multiply two matrices. Do another input");
		return;
	}
	else
	{
		printf("Enter matrix 1: ");
		for(i=0;i<m;i++)
			for(j=0;j<n;j++)
				scanf("%d", &a[i][j]);
		printf("Enter matrix 2: ");
		for(i=0;i<p;i++)
			for(j=0;j<q;j++)
				scanf("%d", &b[i][j]);

		for(i=0;i<m;i++)
			for(j=0;j<n;j++)
				c[i][j] = a[i][j] + b[i][j];
		printf("\nAddition:\n");
		for(i=0;i<m;i++)
		{
			for(j=0;j<n;j++)
				printf("%d ", c[i][j]);
			printf("\n");
		}

		for(i=0;i<m;i++)
			for(j=0;j<q;j++)
				for(k=0;k<p;k++)
					d[i][j] += a[i][k]*b[k][j];
		printf("\nMultiplication:\n");
		for(i=0;i<m;i++)
		{
			for(j=0;j<q;j++)
				printf("%d ", d[i][j]);
			printf("\n");
		}
	}

}
