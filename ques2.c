#include <stdio.h>

 int main()
{
    char string[1000], ch;
    int count = 0;

    printf("Enter a string: ");
    fgets(string, sizeof(string), stdin);

    printf("Enter a character to find its frequency: ");
    scanf("%c", &ch);

    for (int i = 0; string[i] != '\0'; ++i) {
        if (ch == string[i])
            ++count;
    }

    printf("Frequency of %c = %d", ch, count);
    return 0;
}
